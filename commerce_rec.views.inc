<?php

/**
 * @file
 * Views Data
 */

/**
 * Implements hook_views_data_alter().
 */
function commerce_rec_views_data_alter(&$data) {

  $data['commerce_product']['commerce_rec_similarity']['relationship'] = array(
    'group' => t('Commerce Product'),
    'title' => t('Products recomended for this product'),
    'help' => t('Relationship that joins the product with recomended products using the recomender simularity table'),
    'handler' => 'views_handler_relationship_recommender_similarity',
    'base' => 'commerce_product',
    'base field' => 'product_id',
    'recommender app' => 'commerce_rec',
    'source eid' => 'product_id',
  );

  $data['commerce_product']['commerce_rec_prediction']['relationship'] = array(
    'group' => t('Commerce Product'),
    'title' => t('Products recomended for this user'),
    'help' => t('Relationship that joins the user with recomended products using the recomender prediction table'),
    'handler' => 'views_handler_relationship_recommender_prediction',
    'base' => 'users',
    'base field' => 'uid',
    'recommender app' => 'commerce_rec',
    'target eid' => 'product_id',
  );

}
